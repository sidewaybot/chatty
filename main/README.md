# sidewaybot chatty library

Work in progress...



_Note: Although this is based on Vorpal and Inquirer, the Chatty API is not compatible with those of Vorpal and Inquirer._


## Overview

To install this library, run:

```bash
$ npm install @sidewaybot/chatty --save
```

## License

MIT © [Harry Y](mailto:sidewaybot@yahoo.com)
