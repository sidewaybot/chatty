// import { autocomplete } from './autocomplete';

export class Option {

  // tbd.
  private _default: any;
  autocomplete: any;
  flags: string;
  required: boolean;
  optional: boolean;
  bool: boolean;
  long: string;
  short: string;
  description: string;

  /**
   * Initialize a new `Option` instance.
   *
   * @param {String} flags
   * @param {String} description
   * @param {Autocomplete} autocomplete
   * @return {Option}
   * @api public
   */
  constructor(flags: string, description: string, options: any = {}) {   // tbd.
    options = typeof options === 'object' ? options : {
      autocomplete: options
    };

    this._default = options.default ? options.default : null;
    this.autocomplete = options.autocomplete;
    this.flags = flags;
    this.required = (~flags.indexOf('<') != 0);   // tbd.
    this.optional = (~flags.indexOf('[') != 0);   // tbd.
    this.bool = (~flags.indexOf('-no-') == 0);    // tbd.
    let flagArr = flags.split(/[ ,|]+/);    // tbd.
    if (flagArr.length > 1 && !/^[[<]/.test(flagArr[1])) {
      let f1 = flagArr.shift();   // tbd.
      if (f1) {
        this.assignFlag(f1);
      }
    }
    let f2 = flagArr.shift();   // tbd.
    if(f2) {
      this.assignFlag(f2);
    }
    this.description = description || '';
  }

  /**
   * Return option name.
   *
   * @return {String}
   * @api private
   */

  name(): string {
    if (this.long !== undefined) {
      return this.long
        .replace('--', '')
        .replace('no-', '');
    }
    return this.short
      .replace('-', '');
  }

  /**
   * Check if `arg` matches the short or long flag.
   *
   * @param {String} arg
   * @return {Boolean}
   * @api private
   */

  is(arg) {
    return (arg === this.short || arg === this.long);
  }

  /**
   * Assigned flag to either long or short.
   *
   * @param {String} flag
   * @api private
   */

  assignFlag(flag: string) {
    if (flag.startsWith('--')) {
      this.long = flag;
    } else {
      this.short = flag;
    }
  }

  default() {
    return this._default;
  }
}
