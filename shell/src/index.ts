import * as inquirer from 'inquirer';
const vorpal = require('vorpal')();


// Testing...

var question1: inquirer.Question = {
  type: 'confirm',
  name: 'toBeDelivered',
  message: 'Is this for delivery?',
  default: false
};
// inquirer.prompt([question1]).then((answers) => {
//   console.log('Answers: ' + answers);
// });


vorpal
  .command('foo', 'Outputs "bar".')
  .action(function (args, callback) {
    this.log('bar: ' + args);
    callback();
  });

var skiesAlignTonight = true;
vorpal
  .command('fooo', 'Outputs "barrr".')
  .action(function (args) {
    this.log('barrr: ' + args);
    // return new Promise(function (resolve, reject) {
    //   if (skiesAlignTonight) {
    //     resolve();
    //   } else {
    //     reject("Better luck next time");
    //   }
    // };
    return inquirer.prompt([question1]).then((answers) => {
      console.log('Answers: ' + answers);
    });
  });

vorpal
  .command('fo <requiredArg> [optionalArg]')
  .option('-v, --verbose', 'Print foobar instead.')
  .description('Outputs "bo".')
  .alias('foosball')
  .action(function (args, callback) {
    if (args.options.verbose) {
      this.log('fobo: ' + args);
    } else {
      this.log('bo: ' + args);
    }
    callback();
  });

vorpal
  .delimiter('chatty$')
  .show()
  // .parse(process.argv);
