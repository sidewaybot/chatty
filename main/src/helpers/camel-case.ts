export namespace chatty {
  export function camelCase(word: string): string {
    return word.split('-').reduce((str, word) => str + word[0].toUpperCase() + word.slice(1));
  }
}
