# sidewaybot chatty-vorpal library

This is a direct "porting" of [Vorpal](https://github.com/dthree/vorpal) to Typescript.

_Work in progress_

(Note: 
Vorpal is in active development, and 
there is really no easy way to keep this ts version in sync with the original Vorpal library.
Chatty-Vorpal is just an attempt to get a temporary snapshot of the Vorpal js codebase.
The snapshot will be used as a basis for [Chatty](https://github.com/sidewaybot/chatty).)


## Overview


To install this library, run:

```bash
$ npm install @sidewaybot/chatty-vorpal --save
```

## License

MIT © [Harry Y](mailto:sidewaybot@yahoo.com)
