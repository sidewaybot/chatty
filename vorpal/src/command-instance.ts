import { Util } from './util';

export class CommandInstance {

  // tbd:
  args: any;
  callback: any;
  command: any;
  commandObject: any;
  commandWrapper: any;
  downstream: any;
  parent: any;
  session: any;

  constructor(options = {}) {
    // const { command, commandObject, args, commandWrapper, callback, downstream } = options;

    // tbd.
    let opts: any = options;

    this.args = opts.args;
    this.callback = opts.callback;
    this.command = opts.command;
    this.commandObject = opts.commandObject;
    this.commandWrapper = opts.commandWrapper;
    this.downstream = opts.downstream;
    this.parent = opts.commandWrapper.session.parent;
    this.session = opts.commandWrapper.session;
  }

  /**
   * Cancel running command.
   */
  cancel() {
    this.session.emit('vorpal_command_cancel');
  }

  /**
   * Route stdout either through a piped command, or the session's stdout.
   */
  log(...params) {
    const args = (Util as any).fixArgsForApply(params);   // tbd.

    if (!this.downstream) {
      this.session.log(...args);

      return;
    }

    this.session.registerCommand();
    this.downstream.args.stdin = args;

    const onComplete = (error) => {
      if (this.session.isLocal() && error) {
        this.session.log(error.stack || error);
        this.session.parent.emit('client_command_error', {
          command: this.downstream.command,
          error,
        });
      }

      this.session.completeCommand();
    };

    const validate = this.downstream.commandObject._validate;

    if (typeof validate === 'function') {
      try {
        validate.call(this.downstream, this.downstream.args);
      } catch (e) {
        // Log error without piping to downstream on validation error.
        this.session.log(e.toString());

        // tbd.
        onComplete(null);    // tbd.

        return;
      }
    }

    const callback = this.downstream.commandObject._fn;

    if (callback) {
      const response = callback.call(this.downstream, this.downstream.args, onComplete);

      if (response instanceof Promise) {
        response.then(onComplete, onComplete);
      }
    }
  }

  prompt(a, b, c) {
    return this.session.prompt(a, b, c);
  }

  delimiter(a, b, c) {
    return this.session.delimiter(a, b, c);
  }

  help(a, b, c) {
    return this.session.help(a, b, c);
  }

  match(a, b, c) {
    return this.session.match(a, b, c);
  }
}
